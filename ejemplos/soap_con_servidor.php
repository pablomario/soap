<?php
// Creo el servidor soap
$soap_servidor = new SoapServer("http://127.0.0.1/soap/soap_con_wsdl.wsdl");
// Defino el funcionamiento de las funciones
function sumar($operando1,$operando2){
      return $operando1+$operando2;
}
function restar($operando1,$operando2){
      return $operando1-$operando2;
}
// A�ado las funciones al servidor soap
$soap_servidor->AddFunction("sumar");
$soap_servidor->AddFunction("restar");
// Pongo el servidor activo
$soap_servidor->handle();
?>