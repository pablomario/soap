<?php
// Acceso a los datos meteorológicos de las grandes ciudades
$parametros = array(
'CityName' => 'Zaragoza',
'CountryName' => 'Spain'
);
$cliente = new SoapClient('http://www.webservicex.net/globalweather.asmx?WSDL');
$respuesta = $cliente->GetWeather($parametros);
//print_r($respuesta);
$r=$respuesta->GetWeatherResult;
echo $r.'<br>';

// Acceso a la localizacion de una dirección IP
$client = new SoapClient('http://www.webservicex.net/geoipservice.asmx?WSDL');
$result = $client->GetGeoIP(array('IPAddress' => '90.165.103.88'));
//print_r($result);
$r=$result->GetGeoIPResult->CountryName;
echo 'Esa direccion esta en '.$r;
?>