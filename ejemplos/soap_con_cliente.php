<?php
try{
	// Creo el cliente SOAP
	$soap_cliente = new SoapClient('http://127.0.0.1/soap/soap_con_wsdl.wsdl',array('trace' => 1));

	// Uso los metodos remotos como si fueran locales
	$resultado_suma = $soap_cliente->sumar(2.7, 3.5);
	$resultado_resta = $soap_cliente->restar(2.7, 3.5);

	// Muestro los resultados
	echo $resultado_suma." --- ".$resultado_resta;

	// Para ver la respuesta del servidor SOAP podemos a�adir trace() en la creacion del cliente, haciendo una operacion
	$soap_cliente2 = new SoapClient('http://127.0.0.1/soap/soap_con_wsdl.wsdl',array('trace' => 1));
	$resultado2 = $soap_cliente2->sumar(4, 5);
	echo "<br>".$resultado2."<br>";
	echo $soap_cliente2->__getLastRequestHeaders(). "<br>"; // Devuelve los encabezados SOAP de la �ltima petici�n
	echo $soap_cliente2->__getLastRequest() . "<br>"; // Ultima petici�n SOAP
	echo $soap_cliente2->__getLastResponseHeaders(). "<br>"; // Devuelve los encabezados SOAP de la �ltima respuesta
	echo $soap_cliente2->__getLastResponse(). "<br>"; // Devuelve la �ltima respuesta SOAP
} catch (Exception $e) {
	echo 'Error --> '. $e->getMessage();
}
?>