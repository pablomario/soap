<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title> SOAP </title>
	<link rel="stylesheet" href="estilo.css">
</head>
<body>

	<section>


<?php
	try{
		function desplegable($cadena){		
			$val = explode("*",$cadena);	
			echo '<select name=id>';	
			foreach($val as $indice=>$x ){
				echo "<option value='".$x."'>".$x."</option>";
			}
			echo '</select>';
		}

		function desplegable2($cadena){		
			$val = explode("*",$cadena);	
			echo '<select name=id2>';	
			foreach($val as $indice=>$x ){
				echo "<option value='".$x."'>".$x."</option>";
			}
			echo '</select>';
		}	
	

		// SABECO OSCAR
			$soap_cliente = new SoapClient('http://192.168.31.3/sabeco/wsdl.wsdl');
			// Uso los metodos remotos como si fueran locales	
			$valores = $soap_cliente->que();
			echo "<article><h1>SABECO</h1>";	
			echo "<form action='action.php?t=s' method='POST'>";	
			desplegable($valores);	
			echo '<input type="submit" value="Consultar"></form></article>';

		// BOLSA PABLO
			$soap_cliente = new SoapClient('http://192.168.31.4/SOAP/bolsa/wsdl.wsdl');
			// Uso los metodos remotos como si fueran locales	
			$valores = $soap_cliente->que();
			echo "<article><h1>BOLSA</h1>";
			echo "<form action='action.php?t=b' method='POST'>";	
			desplegable($valores);	
			echo '<input type="submit" value="Consultar"></form></article>';
				
		// EXAMENES JUAN
			$soap_cliente = new SoapClient('http://192.168.31.5/SOAP/soap_con_wsdl.wsdl');
			// Uso los metodos remotos como si fueran locales	
			$valores = $soap_cliente->que();
			echo "<article><h1>EXAMENES</h1>";
			echo "<form action='action.php?t=e' method='POST'>";	
			desplegable($valores);	
			echo '<input type="submit" value="Consultar"></form></article>';

		// FUTBOL VELEZ
			$soap_cliente = new SoapClient('http://192.168.31.8/SOAP/wsdl.wsdl');
			// Uso los metodos remotos como si fueran locales	
			$valores = $soap_cliente->que();
			echo "<article><h1>FUTBOL</h1>";
			echo "<form action='action.php?t=f' method='POST'>";
			desplegable($valores);
			desplegable2($valores);	
			echo '<input type="submit" value="Consultar"></form></article>';

		// GASOLINA REY
			$soap_cliente = new SoapClient('http://192.168.31.9/SOAP/wsdl.wsdl');
			// Uso los metodos remotos como si fueran locales	
			$valores = $soap_cliente->que();
			echo "<article><h1>GASOFA</h1>";
			echo "<form action='action.php?t=g' method='POST'>";	
			desplegable($valores);	
			echo '<input type="submit" value="Consultar"></form></article>';

		// POKEON SILVIA
			$soap_cliente = new SoapClient('http://192.168.31.10/pokemon/soap_con_wsdl.wsdl');
			// Uso los metodos remotos como si fueran locales	
			$valores = $soap_cliente->que();
			echo "<article><h1>POKEMON</h1>";
			echo "<form action='action.php?t=p' method='POST'>";	
			desplegable($valores);	
			echo '<input type="submit" value="Consultar"></form></article>';

		// METEOROLOGIA MARCOS
			$soap_cliente = new SoapClient('http://192.168.31.11/SOAP/wsdl.wsdl');
			// Uso los metodos remotos como si fueran locales	
			$valores = $soap_cliente->que();
			echo "<article><h1>METEOROLOGIA</h1>";
			echo "<form action='action.php?t=m' method='POST'>";	
			desplegable($valores);	
			echo '<input type="submit" value="Consultar"></form></article>';

		// NOTAS ELISA
			$soap_cliente = new SoapClient('http://192.168.31.12/SOAP/wsdl.wsdl');
			// Uso los metodos remotos como si fueran locales	
			$valores = $soap_cliente->que();
			echo "<article><h1>NOTAS</h1>";
			echo "<form action='action.php?t=n' method='POST'>";	
			desplegable($valores);	
			echo '<input type="submit" value="Consultar"></form></article>';

	} catch (Exception $e) {
		echo 'Error --> '. $e->getMessage();
	}
?>


		
	</section>	
</body>
</html>

