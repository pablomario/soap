-- MySQL Administrator dump 1.4
--
-- ------------------------------------------------------
-- Server version	5.1.50-community


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


--
-- Create schema bolsa
--

CREATE DATABASE IF NOT EXISTS bolsa;
USE bolsa;

--
-- Definition of table `cotizacion`
--

DROP TABLE IF EXISTS `cotizacion`;
CREATE TABLE `cotizacion` (
  `ID` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `valor` varchar(45) NOT NULL,
  `fecha` datetime NOT NULL,
  `precio` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`,`valor`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cotizacion`
--

/*!40000 ALTER TABLE `cotizacion` DISABLE KEYS */;
INSERT INTO `cotizacion` (`ID`,`valor`,`fecha`,`precio`) VALUES 
 (1,'1','2015-01-21 00:00:00','2.884'),
 (2,'1','2015-01-22 00:00:00','2.891'),
 (3,'2','2015-01-21 00:00:00','3.854'),
 (4,'2','2015-01-22 00:00:00','3.801'),
 (5,'3','2015-01-21 00:00:00','12.874'),
 (6,'3','2015-01-22 00:00:00','12.888'),
 (7,'4','2015-01-21 00:00:00','3.254'),
 (8,'4','2015-01-22 00:00:00','3.301'),
 (9,'5','2015-01-21 00:00:00','5.429'),
 (10,'5','2015-01-22 00:00:00','5.500'),
 (11,'6','2015-01-21 00:00:00','17.52'),
 (12,'6','2015-01-22 00:00:00','17.611');
/*!40000 ALTER TABLE `cotizacion` ENABLE KEYS */;


--
-- Definition of table `valores`
--

DROP TABLE IF EXISTS `valores`;
CREATE TABLE `valores` (
  `ID` int(10) unsigned NOT NULL,
  `nombre` varchar(45) NOT NULL,
  `ticker` varchar(45) NOT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `valores`
--

/*!40000 ALTER TABLE `valores` DISABLE KEYS */;
INSERT INTO `valores` (`ID`,`nombre`,`ticker`) VALUES 
 (1,'ABENGOA','ABG.P'),
 (2,'ABERTIS A','ABE'),
 (3,'SABADELL','SAB'),
 (4,'BANKIA','BKIA'),
 (5,'BBVA','BBVA'),
 (6,'INDITEX','ITX');
/*!40000 ALTER TABLE `valores` ENABLE KEYS */;




/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
